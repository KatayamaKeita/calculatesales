package jp.alhinc.katayama_keita.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class CalculateSales {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		String category = "支店";
		String regex = "[0-9]{3}";
		String inFile = "branch.lst";
		String outFile = "branch.out";
		Map <String,String> definition = new LinkedHashMap<String,String> ();
		Map <String,Long> saleMap = new HashMap<String,Long> ();

		if(!Read(args[0],inFile,definition,saleMap,category,regex)){
			return;
		}

		BufferedReader brSale = null;
		File files = new File(args[0]);
		File[] fileList = files.listFiles();//ファイル一覧
		ArrayList<File> saleFile = new ArrayList<File>();//フィルタされたファイル

		for(int i = 0; i < fileList.length ; i++ ) {
			if(fileList[i].isFile() && (fileList[i].getName().matches("[0-9]{8}+.rcd")) ) {
				saleFile.add(fileList[i]);
			}
		}
		for(int i = 0; i < saleFile.size()- 1; ++i ) {
			String[] fileNamberStr = ((saleFile.get(i)).getName()).split("\\.",0);
			String[] fileNamberNextStr = ((saleFile.get(i+1)).getName()).split("\\.",0);
			int fileNamber = Integer.parseInt(fileNamberStr[0]);
			int fileNamberNext = Integer.parseInt(fileNamberNextStr[0]);
			if(fileNamberNext - fileNamber != 1){
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
		for(int i = 0; i < saleFile.size(); ++i){
			try {
				ArrayList<String> saleDetail = new ArrayList<String>();
				FileReader frSale = new FileReader(saleFile.get(i));
				brSale = new BufferedReader(frSale);
				String saleInfo;
				while((saleInfo = brSale.readLine()) != null) {
					saleDetail.add(saleInfo);
				}
				String fileName = (saleFile.get(i)).getName();
				if(saleDetail.size() != 2){
					System.out.println(fileName + "のフォーマットが不正です");
					return;
				}
				if(!definition.containsKey(saleDetail.get(0))){
					System.out.println(fileName + "の支店コードが不正です");
					return;
				}
				if(!saleDetail.get(1).matches("^[0-9]*$")){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				long sale = Long.parseLong(saleDetail.get(1));
				long saleTotal = (saleMap.get(saleDetail.get(0)))+sale;
				if(saleTotal >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				saleMap.put(saleDetail.get(0), saleTotal);
			}catch(IOException e){
				System.out.println("予期せぬエラーが発生しました");
				return;
			}
			finally{
				if(brSale != null) {
					try {
						brSale.close();
					}catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
		if(!export(args[0],definition,saleMap,outFile)){
			return;
		}
	}

	public  static boolean export(String path, Map<String , String> definitionExport , Map<String,Long> saleMapExport , String outFileExport){
		String lineChange = System.getProperty("line.separator");
		BufferedWriter bw = null;
		try{
			File file = new File(path,outFileExport);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for (Entry<String, String> entry : definitionExport.entrySet()) {
				bw.write( entry.getKey() + "," + entry.getValue() + "," + saleMapExport.get(entry.getKey()) + lineChange);
			}
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(bw != null) {
				try {
					bw.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	public  static boolean Read(String path , String inFileImport, Map<String , String> definitionImport , Map<String,Long> saleMapImport , String categoryImport , String regexImport){
		BufferedReader br = null;
		try {
			File file = new File(path,inFileImport);
			if(!file.exists()) {
				System.out.println(categoryImport + "定義ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String branchDefinition;
			while((branchDefinition = br.readLine()) != null) {
				String[] definitionWord = branchDefinition.split(",",0);
				if(!definitionWord[0].matches(regexImport) || definitionWord.length != 2){
					System.out.println(categoryImport + "定義ファイルのフォーマットが不正です");
					return false;
				}
				long firstScore = 0;
				definitionImport.put(definitionWord[0], definitionWord[1]);
				saleMapImport.put(definitionWord[0], firstScore);
			}
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}
		finally{
			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
				}
			}
		}
		return true;
	}
}

